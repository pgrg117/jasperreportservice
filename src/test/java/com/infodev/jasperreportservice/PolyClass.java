package com.infodev.jasperreportservice;

public class PolyClass {

    public static void main(String[] args) {

        /*Shape shape = new Triangle();
        shape.draw();

        Shape shape1 = new Circle();
        shape1.draw();*/


        Injector injector = new Injector(new Triangle());

        Shape shape = injector.getDependency();
        shape.draw();
    }

}

class Injector{
    private Shape shape;

    public Injector(Shape shape) {
        this.shape = shape;
    }

    public Shape getDependency(){
        return this.shape;
    }
}

abstract class Shape{

    public abstract void draw();

}

class Triangle extends Shape{

    @Override
    public void draw() {
        System.out.println("Drew Triangle");
    }
}

class Circle extends Shape{

    @Override
    public void draw() {
        System.out.println("Drew Circle");
    }
}
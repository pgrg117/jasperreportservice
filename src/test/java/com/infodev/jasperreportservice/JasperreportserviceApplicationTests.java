package com.infodev.jasperreportservice;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
class JasperreportserviceApplicationTests {

    @Test
    void contextLoads() {
    }



    @Test
    void test(){
        char ch = 'क';
        System.out.println("char value: " + ch);

        // Converting the character to it's int value
        int a = (ch);
        System.out.println("int value: " + a);
    }
}

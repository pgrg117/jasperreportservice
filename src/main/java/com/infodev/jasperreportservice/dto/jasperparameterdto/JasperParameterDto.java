package com.infodev.jasperreportservice.dto.jasperparameterdto;

import com.infodev.jasperreportservice.entities.jasperfiles.JasperFiles;
import com.infodev.jasperreportservice.entities.jasperparameter.JasperParameters;
import lombok.*;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class JasperParameterDto {

    private Integer id;

    private String parameterName;

    private String keyName;

    private String value;

    private HashMap<String, String> keyValue;

    private String defaultValue;

    private String dataType;

    private JasperFiles jasperFiles;

    private Integer jasperFileId;

    public JasperParameterDto toDto(JasperParameters jasperParameters){
        return JasperParameterDto.builder()
                .id(jasperParameters.getId())
                .keyName(jasperParameters.getKeyName())
                .defaultValue(jasperParameters.getDefaultValue())
                .dataType(jasperParameters.getDataType())
                .jasperFileId(jasperParameters.getJasperFiles() == null ? null : jasperParameters.getJasperFiles().getId())
                .build();
    }
    public List<JasperParameterDto> toDto(List<JasperParameters> jasperParametersList){
        List<JasperParameterDto> jasperParameterDtoList = new ArrayList<>();
        for(JasperParameters jasperParameters : jasperParametersList){
            jasperParameterDtoList.add(toDto(jasperParameters));
        }
        return jasperParameterDtoList;
    }
    public JasperParameters toEntity(JasperParameterDto jasperParameterDto){
        return JasperParameters.builder()
                .keyName(jasperParameterDto.getKeyName())
                .defaultValue(jasperParameterDto.getDefaultValue())
                .parameterName(jasperParameterDto.getParameterName())
                .dataType(jasperParameterDto.getDataType())
                .jasperFiles(jasperParameterDto.getJasperFileId() == null ? null : new JasperFiles(jasperParameterDto.getJasperFileId()))
                .id(jasperParameterDto.getId() == null ? null : jasperParameterDto.getId())
                .build();
    }

}

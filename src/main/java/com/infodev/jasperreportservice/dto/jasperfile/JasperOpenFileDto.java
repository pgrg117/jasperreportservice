package com.infodev.jasperreportservice.dto.jasperfile;

import com.infodev.jasperreportservice.enums.DataSourceType;
import com.infodev.jasperreportservice.enums.ReportFormat;
import lombok.*;

import java.util.HashMap;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class JasperOpenFileDto {
    private Integer jasperFileId;

    private ReportFormat reportFormat;

    private HashMap<String, Object> extraParameters;

    private DataSourceType dataSourceType;

    private String jsonData;

    private String driverClass;

    private String dbUrl;

    private String username;

    private String password;

    public HashMap<String, Object> getExtraParameters() {
        return extraParameters;
    }

    String json = "{\n" +
            "    \"faculty\" : {\n" +
            "        \"subjects\": [\n" +
            "            {\n" +
            "                \"subject_code\" : \"BHM-202\",\n" +
            "                \"subject_name\" : \"MANANGEMENT OF BHM\",\n" +
            "                \"marks\" : [\n" +
            "                    {\n" +
            "                       \"a\" : 10     \n" +
            "                    },\n" +
            "                    {\n" +
            "                        \"a\" : 10     \n" +
            "                     },\n" +
            "                     {\n" +
            "                        \"a\" : 10     \n" +
            "                     }\n" +
            "                ]\n" +
            "            }\n" +
            "        ]\n" +
            "    }\n" +
            "}";
}

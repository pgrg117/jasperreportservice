package com.infodev.jasperreportservice.dto.jasperfile;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.infodev.jasperreportservice.entities.jasperfiles.JasperFiles;
import lombok.*;
import org.springframework.web.multipart.MultipartFile;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Getter
@Setter
@Builder
@AllArgsConstructor
@NoArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
public class JasperFileDto {
    private Integer id;

    private String jrxmlName;

    private String extension;

    private Long size;

    private String filePath;

    private String description;

    private Boolean hasSubReports;

    private MultipartFile jasperFile;

    private List<MultipartFile> subReports;

    private String reportName;

    private List<JasperFileDto> subReportList;

    public JasperFileDto toDto(JasperFiles jasperFiles){
        return JasperFileDto.builder()
                .id(jasperFiles.getId())
                .reportName(jasperFiles.getReportName())
                .jrxmlName(jasperFiles.getJrxmlName())
                .description(jasperFiles.getDescription())
                .extension(jasperFiles.getExtension())
                .hasSubReports(jasperFiles.getHasSubReports())
                .filePath(jasperFiles.getFilePath())
                .size(jasperFiles.getSize())
                .subReportList(jasperFiles.getSubJasperFiles().stream().map(x->{
                    return JasperFileDto.builder()
                            .jrxmlName(x.getJrxmlName())
                            .filePath(x.getFilePath())
                            .build();
                }).collect(Collectors.toList()))
                .build();
    }

    public List<JasperFileDto> toDto(List<JasperFiles> jasperFilesList){
        List<JasperFileDto> jasperFileDtoList = new ArrayList<>();
        for (JasperFiles jasperFile : jasperFilesList){
            jasperFileDtoList.add(toDto(jasperFile));
        }
        return jasperFileDtoList;
    }
}

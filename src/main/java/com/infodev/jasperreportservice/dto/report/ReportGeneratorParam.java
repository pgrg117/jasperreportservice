package com.infodev.jasperreportservice.dto.report;

import com.infodev.jasperreportservice.enums.DataSourceType;
import com.infodev.jasperreportservice.enums.ReportFormat;
import lombok.*;

import java.util.HashMap;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class ReportGeneratorParam {
    private String rawJsonData;
    private String reportName;
    private String reportPath;
    private HashMap<String, Object> parameter;
    private ReportFormat reportFormat;
    private String outputDir;
    private String outputFileName;
    private String driverClass;
    private String url;
    private String username;
    private String password;
    private DataSourceType dataSourceType;
}

package com.infodev.jasperreportservice.enums;

public enum ReportFormat {
    pdf,
    word,
    excel,
    html
}

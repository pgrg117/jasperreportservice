package com.infodev.jasperreportservice.enums;

public enum DataSourceType {
    DATABASE,
    JSON,
    JRBEANS
}

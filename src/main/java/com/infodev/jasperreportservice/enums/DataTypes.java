package com.infodev.jasperreportservice.enums;

public enum DataTypes {
    number,
    string,
    decimalNumber,
}

package com.infodev.jasperreportservice.enums;

public enum ReportAction {
    download,
    view,
    send
}

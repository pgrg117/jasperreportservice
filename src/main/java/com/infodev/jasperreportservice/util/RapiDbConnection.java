package com.infodev.jasperreportservice.util;

import org.hibernate.exception.JDBCConnectionException;
import org.springframework.jdbc.datasource.DriverManagerDataSource;

import java.sql.Connection;
import java.sql.SQLException;

public class RapiDbConnection {

    public static Connection getDbConnection(){
        try {
            DriverManagerDataSource dataSource = new DriverManagerDataSource();
            dataSource.setDriverClassName( "org.postgresql.Driver");
            dataSource.setUrl( "jdbc:postgresql://test-aurora-db-postgres96.cluster-c4edhzqw29dy.ap-south-1.rds.amazonaws.com:5432/los");
            dataSource.setUsername( "root");
            dataSource.setPassword( "NeW1More2SeCure3PaSSWord");
            Connection connectionJasper = dataSource.getConnection();
            return connectionJasper;
        }
        catch (JDBCConnectionException | SQLException ex){
            System.out.println("error on db connection : "+ex.getMessage());
            throw new RuntimeException("error on db connection : "+ex.getMessage());
        }
    }
}

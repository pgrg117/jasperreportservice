package com.infodev.jasperreportservice.util;

import org.hibernate.exception.JDBCConnectionException;
import org.springframework.jdbc.datasource.DriverManagerDataSource;

import java.sql.Connection;
import java.sql.SQLException;

public class DatabaseConnector {

    public static Connection getDbConnection(String driverClass, String url, String username, String password){
        try {
            DriverManagerDataSource dataSource = new DriverManagerDataSource();
            dataSource.setDriverClassName( driverClass);
            dataSource.setUrl(url);
            dataSource.setUsername(username);
            dataSource.setPassword(password);
            Connection connectionJasper = dataSource.getConnection();
            return connectionJasper;
        }
        catch (JDBCConnectionException | SQLException ex){
            System.out.println("error on db connection : "+ex.getMessage());
            throw new RuntimeException("error on db connection : "+ex.getMessage());
        }
    }

}

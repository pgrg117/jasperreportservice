package com.infodev.jasperreportservice.entities.jasperfiles;

import com.infodev.jasperreportservice.abstracts.EntityBasic;
import lombok.*;

import javax.persistence.*;
import java.util.List;

@Entity
@Table(name = "jasper_files")
@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class JasperFiles extends EntityBasic {
    @Id
    @SequenceGenerator(name = "sequence_jasper_files", allocationSize = 1, sequenceName = "sequence_jasper_files")
    @GeneratedValue(generator = "sequence_jasper_files", strategy = GenerationType.SEQUENCE)
    private Integer id;

    private String reportName;

    private String jrxmlName;

    private String extension;

    private Long size;

    private String filePath;

    private String description;

    private Boolean hasSubReports;

    @OneToMany(fetch = FetchType.LAZY)
    @JoinColumn(name = "main_report_id", foreignKey = @ForeignKey(name = "FK_JASPER_FILE_SUBREPORT"))
    private List<JasperFiles> subJasperFiles;

    public JasperFiles(Integer id){
        this.id = id;
    }
}

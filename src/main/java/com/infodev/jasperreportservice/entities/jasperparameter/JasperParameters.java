package com.infodev.jasperreportservice.entities.jasperparameter;

import com.infodev.jasperreportservice.abstracts.EntityBasic;
import com.infodev.jasperreportservice.entities.jasperfiles.JasperFiles;
import lombok.*;

import javax.persistence.*;

@Entity
@Table(name = "jasper_parameters", uniqueConstraints = {@UniqueConstraint(columnNames = {"keyName","jasper_file_id"}, name = "UNIQUE_KEYNAME")})
@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class JasperParameters extends EntityBasic {
    @Id
    @SequenceGenerator(name = "sequence_jasper_parameters", allocationSize = 1, sequenceName = "sequence_jasper_parameters")
    @GeneratedValue(generator = "sequence_jasper_parameters", strategy = GenerationType.SEQUENCE)
    private Integer id;

    private String parameterName;

    private String keyName;

    private String value;

    private String defaultValue;

    private String dataType;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "jasper_file_id", foreignKey = @ForeignKey(name = "FK_JASPER_FILES_JAPSER_PARAMETER"))
    private JasperFiles jasperFiles;

}

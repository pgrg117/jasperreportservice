package com.infodev.jasperreportservice.service.jasperparameterservice;

import com.infodev.jasperreportservice.dto.jasperparameterdto.JasperParameterDto;

import java.util.List;

public interface JasperParameterService {

    JasperParameterDto create(JasperParameterDto jasperParameterDto);

    List<JasperParameterDto> getByJasperFileId(Integer id);

    void deleteJasperParameterById(Integer id);

}

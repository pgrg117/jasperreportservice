package com.infodev.jasperreportservice.service.jasperparameterservice;

import com.infodev.jasperreportservice.dto.jasperparameterdto.JasperParameterDto;
import com.infodev.jasperreportservice.entities.jasperparameter.JasperParameters;
import com.infodev.jasperreportservice.repo.jasperparameter.JasperParameterRepo;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class JapserParamaterServiceImpl implements JasperParameterService{

    private final JasperParameterRepo jasperParameterRepo;

    public JapserParamaterServiceImpl(JasperParameterRepo jasperParameterRepo) {
        this.jasperParameterRepo = jasperParameterRepo;
    }

    @Override
    public JasperParameterDto create(JasperParameterDto jasperParameterDto) {
        JasperParameters jasperParameters = new JasperParameterDto().toEntity(jasperParameterDto);
        jasperParameters = jasperParameterRepo.save(jasperParameters);
        return new JasperParameterDto().toDto(jasperParameters);
    }

    @Override
    public List<JasperParameterDto> getByJasperFileId(Integer id) {
        List<JasperParameters> jasperParametersList = jasperParameterRepo.getByJasperFilesId(id);
        List<JasperParameterDto> jasperParameterDtoList = new JasperParameterDto().toDto(jasperParametersList);
        return jasperParameterDtoList;
    }

    @Override
    public void deleteJasperParameterById(Integer id) {
        JasperParameters jasperParameters = jasperParameterRepo.findById(id).orElseThrow(()-> new RuntimeException("does not exist"));
        jasperParameterRepo.deleteById(id);
    }
}

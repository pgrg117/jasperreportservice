package com.infodev.jasperreportservice.service.JasperFiles.impl;

import com.infodev.jasperreportservice.dto.jasperfile.JasperFileDto;
import com.infodev.jasperreportservice.entities.jasperfiles.JasperFiles;
import com.infodev.jasperreportservice.repo.jasperfile.JasperFileRepo;
import com.infodev.jasperreportservice.service.JasperFiles.JasperFileService;
import org.apache.commons.io.FilenameUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.UUID;

@Service
public class JasperFileServiceImpl implements JasperFileService {

    @Value("${storage.location}")
    public String folderPath;

    /*@Value("${storage.location.linux}")
    public String folderPathLinux;*/

    private final JasperFileRepo jasperFileRepo;

    public JasperFileServiceImpl(JasperFileRepo jasperFileRepo) {
        this.jasperFileRepo = jasperFileRepo;
    }

    @Override
    public JasperFileDto create(JasperFileDto jasperFileDto) {
        if(jasperFileDto.getJasperFile().getSize() > 500000 ){
            throw new RuntimeException(("file exceed 5 MB"));
        }
        String extensions[] = {"jrxml", "jasper"};
        if(!Arrays.asList(extensions).contains(FilenameUtils.getExtension(jasperFileDto.getJasperFile().getOriginalFilename()))){
            throw new RuntimeException(("File extension ")+" not accepted");
        }
        List<JasperFiles> jasperFilesList = new ArrayList<>();
        if (jasperFileDto.getJasperFile() != null){
            String mainReportPath = uploadDocument(jasperFileDto.getJasperFile());
            if(jasperFileDto.getHasSubReports()){
                for (MultipartFile file: jasperFileDto.getSubReports()) {
                    String filePathOfSubReport = uploadDocument(file);
                    JasperFiles jasperFiles = JasperFiles.builder()
                            .jrxmlName(file.getOriginalFilename())
                            .extension(FilenameUtils.getExtension(file.getOriginalFilename()))
                            .filePath(filePathOfSubReport)
                            .build();
                     jasperFiles = jasperFileRepo.save(jasperFiles);
                     jasperFilesList.add(jasperFiles);
                }
            }
            JasperFiles jasperMainFiles = JasperFiles.builder()
                    .filePath(mainReportPath)
                    .reportName(jasperFileDto.getReportName())
                    .jrxmlName(jasperFileDto.getJasperFile().getOriginalFilename())
                    .description(jasperFileDto.getDescription())
                    .hasSubReports(jasperFileDto.getHasSubReports())
                    .size(jasperFileDto.getJasperFile().getSize())
                    .extension(FilenameUtils.getExtension(jasperFileDto.getJasperFile().getOriginalFilename()))
                    .subJasperFiles(jasperFilesList)
                    .build();
            jasperFileRepo.save(jasperMainFiles);
        }
        return null;
    }

    @Override
    public void delete(Integer id) {
        JasperFiles jasperFiles = jasperFileRepo.findById(id).orElseThrow(()-> new RuntimeException("file not found"));
        deleteDocument(jasperFiles.getFilePath());
        if(jasperFiles.getHasSubReports()){
            for(JasperFiles file : jasperFiles.getSubJasperFiles()){
                deleteDocument(file.getFilePath());
            }
        }
        jasperFileRepo.deleteById(id);
    }

    @Override
    public List<JasperFileDto> findAll() {
        return new JasperFileDto().toDto(jasperFileRepo.findAll());
    }

    @Override
    public JasperFileDto findById(Integer id) {
        return new JasperFileDto().toDto(jasperFileRepo.findById(id).orElseThrow(()-> new RuntimeException("does not exist")));
    }

    public String uploadDocument(MultipartFile file) {
        String uuid = UUID.randomUUID().toString().replace("-", " ").substring(0, 8);
        String temp = uuid + "_" + file.getOriginalFilename();
        String fileName = StringUtils.cleanPath(temp);
        String folderPath = null;
        String osName = System.getProperty("os.name");
        switch (System.getProperty("os.name")) {
//            case "Linux":  folderPath = this.folderPathLinux;
//                break;
            case "Windows 10":  folderPath = this.folderPath;
                break;
        }
        File directory = new File(folderPath);
        if (!directory.exists()) {
            directory.mkdirs();
        }
        try {
            //document upload in specific location
            Path path = Paths.get(folderPath + File.separator + fileName);
            Files.copy(file.getInputStream(), path, StandardCopyOption.REPLACE_EXISTING);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return folderPath + File.separator + fileName;
    }
    public void deleteDocument(String path) {
        File file = new File(folderPath);
        if (file.exists()) {
            file.delete();
            System.out.println("success : document service ->>  file deleted successfully");
        } else {
            System.out.println("info : document service ->>  file not found");
        }
    }
}

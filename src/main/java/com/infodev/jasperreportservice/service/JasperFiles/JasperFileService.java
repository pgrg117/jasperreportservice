package com.infodev.jasperreportservice.service.JasperFiles;

import com.infodev.jasperreportservice.dto.jasperfile.JasperFileDto;

import java.util.List;

public interface JasperFileService {
    JasperFileDto create(JasperFileDto jasperFileDto);

    void delete(Integer id);

    List<JasperFileDto> findAll();

    JasperFileDto findById(Integer id);

}

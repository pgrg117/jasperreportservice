package com.infodev.jasperreportservice.service.jasperreport;

import com.infodev.jasperreportservice.dto.jasperfile.JasperOpenFileDto;
import com.infodev.jasperreportservice.dto.report.ReportDto;

public interface JasperReportService {

    ReportDto getReport(JasperOpenFileDto jasperOpenFileDto);

}

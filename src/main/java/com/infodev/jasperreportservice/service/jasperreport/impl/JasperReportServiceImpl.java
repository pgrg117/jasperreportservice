package com.infodev.jasperreportservice.service.jasperreport.impl;

import com.infodev.jasperreportservice.dto.jasperfile.JasperFileDto;
import com.infodev.jasperreportservice.dto.jasperfile.JasperOpenFileDto;
import com.infodev.jasperreportservice.dto.report.ReportDto;
import com.infodev.jasperreportservice.dto.report.ReportGeneratorParam;
import com.infodev.jasperreportservice.enums.DataSourceType;
import com.infodev.jasperreportservice.enums.ReportFormat;
import com.infodev.jasperreportservice.service.JasperFiles.JasperFileService;
import com.infodev.jasperreportservice.service.jasperreport.JasperReportService;
import com.infodev.jasperreportservice.util.DatabaseConnector;
import com.infodev.jasperreportservice.util.RapiDbConnection;
import lombok.extern.slf4j.Slf4j;
import net.sf.jasperreports.engine.*;
import net.sf.jasperreports.engine.data.JsonDataSource;
import net.sf.jasperreports.engine.export.HtmlExporter;
import net.sf.jasperreports.engine.export.ooxml.JRDocxExporter;
import net.sf.jasperreports.engine.fill.JRSwapFileVirtualizer;
import net.sf.jasperreports.engine.util.JRConcurrentSwapFile;
import net.sf.jasperreports.export.Exporter;
import net.sf.jasperreports.export.SimpleExporterInput;
import net.sf.jasperreports.export.SimpleHtmlExporterOutput;
import net.sf.jasperreports.export.SimpleOutputStreamExporterOutput;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.FilenameUtils;
import org.apache.tomcat.util.codec.binary.Base64;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.ResourceLoader;
import org.springframework.stereotype.Service;

import java.io.*;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.sql.Connection;
import java.util.HashMap;
@Slf4j
@Service
public class JasperReportServiceImpl implements JasperReportService {

    @Value("${storage.location}")
    public String directoryPath;
    @Value("${storage.temp}")
    public String tempLocation;
    @Value("${storage.save}")
    public String saveLocation;
    @Value("${storage.pdf}")
    public String pdfLocation;
    @Value("${storage.word}")
    public String wordLocation;
    @Value("${storage.html}")
    public String htmlLocation;
//    @Value("${storage.root}")
//    public String linuxRoot;
    @Value("${storage.rootwin}")
    public String winRoot;
    @Autowired
    private ResourceLoader resourceLoader;

    @Autowired
    private JasperFileService jasperFileService;

    @Override
    public ReportDto getReport(JasperOpenFileDto jasperOpenFileDto) {
        JasperFileDto jasperFiles = jasperFileService.findById(jasperOpenFileDto.getJasperFileId());
        HashMap<String, Object> parameter = new HashMap<>();
        String resourcePath = null;
        switch (System.getProperty("os.name")) {
//            case "Linux":
//                resourcePath = linuxRoot;
//                break;
            case "Windows 10":
                resourcePath = winRoot;
                break;
        }
        ReportGeneratorParam reportGeneratorParam = ReportGeneratorParam.builder()
                .reportPath(jasperFiles.getFilePath())
                .outputFileName(jasperFiles.getReportName().replace(" ", "_"))
                .reportFormat(jasperOpenFileDto.getReportFormat())
                .reportName(jasperFiles.getReportName())
                .parameter(jasperOpenFileDto.getExtraParameters())
                .build();
        if (reportGeneratorParam.getReportFormat().equals(ReportFormat.pdf)) {
            reportGeneratorParam.setOutputDir(resourcePath + File.separator + "saved/pdf");
        } else if (reportGeneratorParam.getReportFormat().equals(ReportFormat.word)) {
            reportGeneratorParam.setOutputDir(resourcePath + File.separator + "saved/word");
        } else if (reportGeneratorParam.getReportFormat().equals(ReportFormat.html)) {
            reportGeneratorParam.setOutputDir(resourcePath + File.separator + "saved/html");
        }

        switch (jasperOpenFileDto.getDataSourceType()){
            case JSON:
                reportGeneratorParam.setRawJsonData(jasperOpenFileDto.getJsonData());
                reportGeneratorParam.setDataSourceType(DataSourceType.JSON);
                break;
            case DATABASE:
                reportGeneratorParam.setDriverClass(jasperOpenFileDto.getDriverClass());
                reportGeneratorParam.setUrl(jasperOpenFileDto.getDbUrl());
                reportGeneratorParam.setUsername(jasperOpenFileDto.getUsername());
                reportGeneratorParam.setPassword(jasperOpenFileDto.getPassword());
                reportGeneratorParam.setDataSourceType(DataSourceType.DATABASE);
                break;
        }
        ReportDto reportDto = generateReport(reportGeneratorParam);
        return reportDto;
    }

    public ReportDto generateReport(ReportGeneratorParam reportGeneratorParam) {
        JasperPrint jasperPrint;
        JRSwapFileVirtualizer virtualizer = null;
        FileOutputStream outputStream = null;
        String fileName = null;
        byte[] fileByte = null;
        String base64String = "";
        byte[] encodedBase64 = null;
        String htmlContent = "";
        makeDir(pdfLocation);
        makeDir(wordLocation);
        makeDir(htmlLocation);
        HashMap<String, Object> parameter = new HashMap<>();
        try {


            File jrxmlFileObj = new File(reportGeneratorParam.getReportPath());
            InputStream inputStream = FileUtils.openInputStream(jrxmlFileObj);
//            System.out.println(reportGeneratorParam.getReportName() + " jasper file is loaded ");
            log.info(reportGeneratorParam.getReportName() + " jasper file is loaded ");
            fileName = FilenameUtils.removeExtension(reportGeneratorParam.getReportName());

            JRConcurrentSwapFile swapFile = new JRConcurrentSwapFile(tempLocation, 1024, 100);
            virtualizer = new JRSwapFileVirtualizer(50, swapFile, true);
            parameter.put(JRParameter.REPORT_VIRTUALIZER, virtualizer);
            JasperReport jasperReport = JasperCompileManager.compileReport(inputStream);
            //Merge maps
            HashMap<String, Object> map3 = new HashMap<>();
            map3.putAll(parameter);
            switch (reportGeneratorParam.getDataSourceType()){
                case DATABASE:
                    /** Get data base connection object for report
                     * */
                    Connection connectionJasper = DatabaseConnector.getDbConnection(
                            reportGeneratorParam.getDriverClass(),
                            reportGeneratorParam.getUrl(),
                            reportGeneratorParam.getUsername(),
                            reportGeneratorParam.getPassword());
                    jasperPrint = JasperFillManager.fillReport(jasperReport, map3, connectionJasper);
                    break;
                case JSON:
                    ByteArrayInputStream jsonDataStream = new ByteArrayInputStream(reportGeneratorParam.getRawJsonData().getBytes());
                    JsonDataSource jds = new JsonDataSource(jsonDataStream);
                    jasperPrint = JasperFillManager.fillReport(jasperReport, map3, jds);
                    break;
                default:
                    throw new RuntimeException("NOT VALID");
            }
            if (reportGeneratorParam.getParameter() != null) {
                map3.putAll(reportGeneratorParam.getParameter());
            }

            if (reportGeneratorParam.getReportFormat() == ReportFormat.pdf) {
                fileName = reportGeneratorParam.getOutputFileName() + "_" + fileName + ".pdf";
                File outputFile = new File(reportGeneratorParam.getOutputDir() + File.separator + fileName);
                outputFile.createNewFile();
                outputStream = new FileOutputStream(outputFile);
                JasperExportManager.exportReportToPdfStream(jasperPrint, outputStream);
                fileByte = Files.readAllBytes(Paths.get(reportGeneratorParam.getOutputDir() + File.separator + fileName));
                outputStream.close();
            }
            if (reportGeneratorParam.getReportFormat() == ReportFormat.word) {
                fileName = reportGeneratorParam.getOutputFileName() + "_" + fileName + ".docx";
                File outputFile = new File(reportGeneratorParam.getOutputDir() + File.separator + fileName);
                Exporter exporter = new JRDocxExporter();
                exporter.setExporterInput(new SimpleExporterInput(jasperPrint));
                exporter.setExporterOutput(new SimpleOutputStreamExporterOutput(outputFile));
                exporter.exportReport();
                fileByte = Files.readAllBytes(Paths.get(reportGeneratorParam.getOutputDir() + File.separator + fileName));
            }
            if (reportGeneratorParam.getReportFormat() == ReportFormat.html) {
                fileName = reportGeneratorParam.getOutputFileName() + "_" + fileName + ".html";
                File outputFile = new File(reportGeneratorParam.getOutputDir() + File.separator + fileName);
                Exporter exporter = new HtmlExporter();
                exporter.setExporterInput(new SimpleExporterInput(jasperPrint));
                exporter.setExporterOutput(new SimpleHtmlExporterOutput(outputFile));
                exporter.exportReport();
                File file = new File(reportGeneratorParam.getOutputDir() + File.separator + fileName);
                try {
                    htmlContent = FileUtils.readFileToString(file, StandardCharsets.UTF_8);
                } catch (IOException e) {
                    e.printStackTrace();
                }
                file.deleteOnExit();
//                System.out.println(content);
            }
            inputStream.close();
            if (!reportGeneratorParam.getReportFormat().equals(ReportFormat.html)) {
                encodedBase64 = Base64.encodeBase64(fileByte);
                base64String = new String(encodedBase64);
            }
            log.info("Report Generation Complete Successfully");
        } catch (Exception jrex) {
            log.info("Report Generation Complete Failed");
            jrex.printStackTrace();
            return null;
        } finally {
            if (virtualizer != null) {
                virtualizer.cleanup();
            }
        }
        return new ReportDto().builder()
                .base64Content(base64String)
                .outputPath(reportGeneratorParam.getOutputDir() + File.separator + fileName)
                .htmlContent(htmlContent)
                .build();
    }

    public void makeDir(String directoryPath) {
        File file = new File(directoryPath);
        if (!file.exists()) {
            file.mkdirs();
        }
        File tempFileDirectory = new File(tempLocation);
        if (!tempFileDirectory.exists()) {
            tempFileDirectory.mkdirs();
        }
    }
}

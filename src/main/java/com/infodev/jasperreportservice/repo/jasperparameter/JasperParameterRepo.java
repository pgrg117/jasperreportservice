package com.infodev.jasperreportservice.repo.jasperparameter;

import com.infodev.jasperreportservice.entities.jasperparameter.JasperParameters;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface JasperParameterRepo extends JpaRepository<JasperParameters, Integer> {

    @Query(nativeQuery = true, value = "select * FROM jasper_parameters where jasper_file_id = ?1")
    List<JasperParameters> getByJasperFilesId(Integer jasperFileId);

}

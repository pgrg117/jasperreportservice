package com.infodev.jasperreportservice.repo.jasperfile;

import com.infodev.jasperreportservice.entities.jasperfiles.JasperFiles;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface JasperFileRepo extends JpaRepository<JasperFiles,Integer> {

}

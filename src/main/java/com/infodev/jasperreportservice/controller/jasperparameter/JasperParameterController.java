package com.infodev.jasperreportservice.controller.jasperparameter;

import com.infodev.jasperreportservice.abstracts.BaseController;
import com.infodev.jasperreportservice.dto.jasperfile.JasperFileDto;
import com.infodev.jasperreportservice.dto.jasperparameterdto.JasperParameterDto;
import com.infodev.jasperreportservice.service.jasperparameterservice.JasperParameterService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/jasper-parameter")
public class JasperParameterController extends BaseController {

    @Autowired
    private JasperParameterService jasperParameterService;

    @PostMapping
    public ResponseEntity<?> createParameter(@RequestBody JasperParameterDto jasperParameterDto) {
        return new ResponseEntity<>(successResponse("Jasper Parameter created",
                jasperParameterService.create(jasperParameterDto)), HttpStatus.OK);
    }

    @GetMapping("/jasper-file/{id}")
    public ResponseEntity<?> getParameters(@PathVariable("id") Integer jasperId) {
        return new ResponseEntity<>(successResponse("Jasper Parameter fetched",
                jasperParameterService.getByJasperFileId(jasperId)), HttpStatus.OK);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<?> deleteParameter(@PathVariable("id") Integer parameterId) {
        jasperParameterService.deleteJasperParameterById(parameterId);
        return new ResponseEntity<>(successResponse("Jasper Parameter deleted",
                null), HttpStatus.OK);
    }
}

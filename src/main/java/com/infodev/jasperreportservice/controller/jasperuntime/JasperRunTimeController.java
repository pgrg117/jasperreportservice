package com.infodev.jasperreportservice.controller.jasperuntime;

import com.infodev.jasperreportservice.abstracts.BaseController;
import com.infodev.jasperreportservice.dto.jasperfile.JasperOpenFileDto;
import com.infodev.jasperreportservice.dto.report.ReportDto;
import com.infodev.jasperreportservice.service.jasperreport.JasperReportService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/jasper")
public class JasperRunTimeController extends BaseController {

    @Autowired
    private JasperReportService jasperReportService;

    @PostMapping("/run-jasper-file")
    public ResponseEntity<?> runJasperReportFile(@RequestBody JasperOpenFileDto jasperOpenFileDto) {
        ReportDto reportDto = jasperReportService.getReport(jasperOpenFileDto);
        if (reportDto != null) {
            return new ResponseEntity<>(successResponse("Jasper file execution complete",
                    jasperReportService.getReport(jasperOpenFileDto)), HttpStatus.OK);
        }
        else {
            return new ResponseEntity<>(errorResponse("Jasper file execution failed",
                    null), HttpStatus.OK);
        }
    }
}

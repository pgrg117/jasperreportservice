package com.infodev.jasperreportservice.controller.jasperfile;

import com.infodev.jasperreportservice.abstracts.BaseController;
import com.infodev.jasperreportservice.dto.jasperfile.JasperFileDto;
import com.infodev.jasperreportservice.dto.jasperfile.JasperOpenFileDto;
import com.infodev.jasperreportservice.service.JasperFiles.JasperFileService;
import com.infodev.jasperreportservice.service.jasperreport.JasperReportService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/jasper-file")
public class JasperFileController extends BaseController {

    @Autowired
    private JasperFileService jasperFileService;
    @Autowired
    private JasperReportService jasperReportService;

    @PostMapping
    public ResponseEntity<?> createJasperFile(@ModelAttribute JasperFileDto jasperFileDto) {
        return new ResponseEntity<>(successResponse("Jasper Upload complete",
                jasperFileService.create(jasperFileDto)), HttpStatus.OK);
    }

    @GetMapping
    public ResponseEntity<?> geAll() {
        return new ResponseEntity<>(successResponse("All files Fetched",
                jasperFileService.findAll()), HttpStatus.OK);
    }

    @GetMapping("/{id}")
    public ResponseEntity<?> getById(@PathVariable("id") Integer id) {
        return new ResponseEntity<>(successResponse("file Fetched",
                jasperFileService.findById(id)), HttpStatus.OK);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<?> deleteById(@PathVariable("id") Integer id) {
        jasperFileService.delete(id);
        return new ResponseEntity<>(successResponse("file Fetched",
                null), HttpStatus.OK);
    }
}

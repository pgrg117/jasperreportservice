This is a Jasper Repoting Service made with Spring boot.
This service saves any Jasper File i.e. .jrxml file and save it to database.
The saved jasper files will then be used with Spring Boot to generated report.

To use this service go through the controllers (APIs).
